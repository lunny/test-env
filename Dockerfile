FROM golang:1.16

# upgrade git to v2.30.2
RUN curl -SL https://github.com/git/git/archive/v2.30.2.tar.gz \
    | tar -xzv -C /go \
    && apt-get update \
    && apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev \
    && make -C /go/git-2.30.2 prefix=/usr all \
    && make -C /go/git-2.30.2 prefix=/usr install \
    && rm -rf /go/git-2.30.2 \
# install git-lfs
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y git-lfs \
# install golangci-lint
    && export BINARY="golangci-lint" \
    && curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.37.0